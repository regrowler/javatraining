package com.ikosmov.java8.optional;

import java.util.Optional;

public class Main {
    public static void main(String[] ar){
        CacheMap m=new CacheMap();
        m.putObject("1","2");
        m.putObject("2","3");
        m.putObject("3","4");
        try {
            Thread.sleep(200 );
            System.out.println("sleep2000");
            m.putObject("tr","asd");
            m.putObject("3adw","4asd");
//            Thread.sleep(4000);
            System.out.println("sleep4000");
            m.invalidate();
            Optional u=m.getObject("uuu");

            System.out.println(u.orElse("asd"));
        }catch (InterruptedException e){e.printStackTrace();}


    }
}
