package com.ikosmov.java8.optional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class CacheMap {
    private HashMap<Object, ObjectWrapper> m;

    public CacheMap() {
        m = new HashMap<>();
    }

    public void putObject(Object key, Object value) {
        m.put(key, new ObjectWrapper(value));
    }

    public void clear() {
        m.clear();
    }

    public Optional<Object> getObject(Object key) throws IllegalArgumentException {
        if (key == null) throw new IllegalArgumentException();

        if (m.get(key) == null) {
            return Optional.empty();

        }
        return Optional.of(m.get(key).object);
    }

    private class ObjectWrapper {
        Object object;
        long time;

        public ObjectWrapper(Object object) {
            this.object = object;
            time = System.currentTimeMillis();
        }
    }

    public void invalidate() {
        List<Object> toRemove = new ArrayList<>();
        m.forEach(((o, objectWrapper) -> {
            long d = System.currentTimeMillis() - objectWrapper.time;
            if (d > (5 * 60 * 1000)) {
                toRemove.add(o);
                System.out.println("deletedn " + o.toString());
            }
        }));
        toRemove.forEach((o -> m.remove(o)));
    }
}
