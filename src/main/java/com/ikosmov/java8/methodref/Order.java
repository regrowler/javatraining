package com.ikosmov.java8.methodref;

public class Order {
    public final OrderStatus status;
    public final int id;
    public Order(OrderStatus status, int id) {
        this.status = status;
        this.id=id;
    }
    public enum OrderStatus {
        NOT_STARTED, PROCESSING, COMPLETED
    }

    @Override
    public String toString() {
        return status.toString()+id;
    }
    public boolean parity(){return id%2==0;}
}
