package com.ikosmov.java8.methodref;



import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println("Stream!");
        Collection<String> c = Collections.EMPTY_LIST;
        List<String> list = new ArrayList<>(c);

        List<Order> orders = new ArrayList<>();
        for(int i=1;i<50;i++) {
            orders.add(new Order(Order.OrderStatus.COMPLETED,i));
        }
        ArrayList<Order> list1=(ArrayList<Order>) orders.stream()
                .filter(order -> order.status == Order.OrderStatus.COMPLETED)
                .filter(Order::parity)
                .collect(Collectors.toList());
        System.out.println(Arrays.toString(list1.toArray()));
    }
}
