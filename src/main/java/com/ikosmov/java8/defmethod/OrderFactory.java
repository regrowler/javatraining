package com.ikosmov.java8.defmethod;

public interface OrderFactory {
    public default Order createCompleted(int id){
        return createOrder(Order.OrderStatus.COMPLETED,id);
    }
    public default Order createProcessing(int id){
        return createOrder(Order.OrderStatus.PROCESSING,id);
    }
    public default Order createNotStarted(int id){
        return createOrder(Order.OrderStatus.NOT_STARTED,id);
    }
    public Order createOrder(Order.OrderStatus status,int id);
}
