package com.ikosmov.java8.defmethod;

public class Main {
    public static void main(String[] a){
        OrderFactory factory=new OrderFactoryImplementation();
        Order order=factory.createCompleted(5);
        System.out.println(order.toString());
    }
}
