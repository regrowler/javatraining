package com.ikosmov.java8.defmethod;

public class Order {
    public final OrderStatus status;
    public int id;
    public Order(OrderStatus status, int id) {
        this.status = status;
        this.id=id;
    }

    public Order(OrderStatus status) {
        this.status = status;
        id=-1;
    }

    public enum OrderStatus {
        NOT_STARTED, PROCESSING, COMPLETED
    }

    @Override
    public String toString() {
        return status.toString()+id;
    }
}
