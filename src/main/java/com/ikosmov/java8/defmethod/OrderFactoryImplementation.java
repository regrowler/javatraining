package com.ikosmov.java8.defmethod;

public class OrderFactoryImplementation implements OrderFactory {

    @Override
    public Order createOrder(Order.OrderStatus status,int id) {
        return new Order(status,id);
    }
}
