package com.ikosmov.markup.xml;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.events.Attribute;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    public static void main(String[] a) {
        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            String link = "https://websvcgatewayx2.frbny.org/autorates_cpff_external/services/v1_0/cpff/xml/retrieveCurrentCPFFUNCP";

            DefaultHandler defaultHandler = new XMLHandler();
            sp.parse(new File("example.xml"), defaultHandler);
            int y = 0;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }

    }

    private static class XMLHandler extends DefaultHandler {
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            int y = 0;
            String obs = attributes.getValue("OBS_STATUS");
            if (obs != null && obs.equals("M")) {
                System.out.println("found obs status m");
                throw new SAXException();
            }
        }

        @Override
        public void endDocument() throws SAXException {
            super.endDocument();
            System.out.println("obs status m not found");
            throw new SAXException("obs status m not found");
        }
    }
}
