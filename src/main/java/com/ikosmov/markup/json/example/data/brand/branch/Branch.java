
package com.ikosmov.markup.json.example.data.brand.branch;

import com.ikosmov.markup.json.example.data.brand.OtherServiceAndFacility;
import com.ikosmov.markup.json.example.data.brand.branch.availibility.Availability;
import com.ikosmov.markup.json.example.data.brand.branch.contact_info.ContactInfo;
import com.ikosmov.markup.json.example.data.brand.branch.postal_address.PostalAddress;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class Branch {

    private String identification;
    private String sequenceNumber;
    private String name;
    private String type;
    private List<String> customerSegment = null;
    private List<String> accessibility = null;
    private List<OtherServiceAndFacility> otherServiceAndFacility = null;
    private Availability availability;
    private List<ContactInfo> contactInfo = null;
    private PostalAddress postalAddress;

    @JsonProperty("Identification")
    public String getIdentification() {
        return identification;
    }

    @JsonProperty("Identification")
    public void setIdentification(String identification) {
        this.identification = identification;
    }

    @JsonProperty("SequenceNumber")
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    @JsonProperty("SequenceNumber")
    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("Type")
    public String getType() {
        return type;
    }

    @JsonProperty("Type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("CustomerSegment")
    public List<String> getCustomerSegment() {
        return customerSegment;
    }

    @JsonProperty("CustomerSegment")
    public void setCustomerSegment(List<String> customerSegment) {
        this.customerSegment = customerSegment;
    }

    @JsonProperty("Accessibility")
    public List<String> getAccessibility() {
        return accessibility;
    }

    @JsonProperty("Accessibility")
    public void setAccessibility(List<String> accessibility) {
        this.accessibility = accessibility;
    }

    @JsonProperty("OtherServiceAndFacility")
    public List<OtherServiceAndFacility> getOtherServiceAndFacility() {
        return otherServiceAndFacility;
    }

    @JsonProperty("OtherServiceAndFacility")
    public void setOtherServiceAndFacility(List<OtherServiceAndFacility> otherServiceAndFacility) {
        this.otherServiceAndFacility = otherServiceAndFacility;
    }

    @JsonProperty("Availability")
    public Availability getAvailability() {
        return availability;
    }

    @JsonProperty("Availability")
    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    @JsonProperty("ContactInfo")
    public List<ContactInfo> getContactInfo() {
        return contactInfo;
    }

    @JsonProperty("ContactInfo")
    public void setContactInfo(List<ContactInfo> contactInfo) {
        this.contactInfo = contactInfo;
    }

    @JsonProperty("PostalAddress")
    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    @JsonProperty("PostalAddress")
    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }


}
