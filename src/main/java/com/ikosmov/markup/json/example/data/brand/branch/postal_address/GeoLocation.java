
package com.ikosmov.markup.json.example.data.brand.branch.postal_address;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class GeoLocation {

    private GeographicCoordinates geographicCoordinates;
    @JsonProperty("GeographicCoordinates")
    public GeographicCoordinates getGeographicCoordinates() {
        return geographicCoordinates;
    }
    @JsonProperty("GeographicCoordinates")
    public void setGeographicCoordinates(GeographicCoordinates geographicCoordinates) {
        this.geographicCoordinates = geographicCoordinates;
    }


}
