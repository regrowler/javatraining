
package com.ikosmov.markup.json.example.data.brand.branch.availibility;

import org.codehaus.jackson.annotate.JsonProperty;

public class Availability {

    private StandardAvailability standardAvailability;
    @JsonProperty("StandardAvailability")
    public StandardAvailability getStandardAvailability() {
        return standardAvailability;
    }
    @JsonProperty("StandardAvailability")
    public void setStandardAvailability(StandardAvailability standardAvailability) {
        this.standardAvailability = standardAvailability;
    }


}
