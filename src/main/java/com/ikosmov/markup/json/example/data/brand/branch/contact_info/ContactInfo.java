
package com.ikosmov.markup.json.example.data.brand.branch.contact_info;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class ContactInfo {

    private String contactType;
    private String contactContent;

    @JsonProperty("ContactType")
    public String getContactType() {
        return contactType;
    }

    @JsonProperty("ContactType")
    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    @JsonProperty("ContactContent")
    public String getContactContent() {
        return contactContent;
    }

    @JsonProperty("ContactContent")
    public void setContactContent(String contactContent) {
        this.contactContent = contactContent;
    }


}
