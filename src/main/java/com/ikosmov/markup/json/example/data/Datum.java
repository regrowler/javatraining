
package com.ikosmov.markup.json.example.data;

import com.ikosmov.markup.json.example.data.brand.Brand;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class Datum {

    private List<Brand> brand = null;
    @JsonProperty("Brand")
    public List<Brand> getBrand() {
        return brand;
    }
    @JsonProperty("Brand")
    public void setBrand(List<Brand> brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Datum{\n" +
                "brand=" + brand +
                "\n]";
    }
}
