
package com.ikosmov.markup.json.example.data.brand;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class OtherServiceAndFacility {

    private String code;
    private String name;
    @JsonProperty("Code")
    public String getCode() {
        return code;
    }
    @JsonProperty("Code")
    public void setCode(String code) {
        this.code = code;
    }
    @JsonProperty("Name")
    public String getName() {
        return name;
    }
    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

}
