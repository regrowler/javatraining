
package com.ikosmov.markup.json.example.data.brand.branch.postal_address;

import com.ikosmov.markup.json.example.data.brand.branch.postal_address.GeoLocation;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostalAddress {

    private List<String> addressLine = null;
    private String townName;
    private List<String> countrySubDivision = null;
    private String country;
    private String postCode;
    private GeoLocation geoLocation;

    @JsonProperty("AddressLine")
    public List<String> getAddressLine() {
        return addressLine;
    }

    @JsonProperty("AddressLine")
    public void setAddressLine(List<String> addressLine) {
        this.addressLine = addressLine;
    }

    @JsonProperty("TownName")
    public String getTownName() {
        return townName;
    }

    @JsonProperty("TownName")
    public void setTownName(String townName) {
        this.townName = townName;
    }

    @JsonProperty("CountrySubDivision")
    public List<String> getCountrySubDivision() {
        return countrySubDivision;
    }

    @JsonProperty("CountrySubDivision")
    public void setCountrySubDivision(List<String> countrySubDivision) {
        this.countrySubDivision = countrySubDivision;
    }

    @JsonProperty("Country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("Country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("PostCode")
    public String getPostCode() {
        return postCode;
    }

    @JsonProperty("PostCode")
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @JsonProperty("GeoLocation")
    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    @JsonProperty("GeoLocation")
    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

}
