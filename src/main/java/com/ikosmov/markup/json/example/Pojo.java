
package com.ikosmov.markup.json.example;

import com.ikosmov.markup.json.example.data.Datum;

import java.util.List;

public class Pojo {

    private List<Datum> data = null;
    public List<Datum> getData() {
        return data;
    }
    public void setData(List<Datum> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Pojo{\n" +
                "data=\n" + data +
                "\n}";
    }
}
