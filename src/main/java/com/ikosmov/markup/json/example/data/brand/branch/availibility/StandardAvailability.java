
package com.ikosmov.markup.json.example.data.brand.branch.availibility;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StandardAvailability {

    private List<Day> day = null;
    @JsonProperty("Day")
    public List<Day> getDay() {
        return day;
    }
    @JsonProperty("Day")
    public void setDay(List<Day> day) {
        this.day = day;
    }


}
