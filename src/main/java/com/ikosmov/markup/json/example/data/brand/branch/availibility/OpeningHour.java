
package com.ikosmov.markup.json.example.data.brand.branch.availibility;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class OpeningHour {

    private String openingTime;
    private String closingTime;
    @JsonProperty("OpeningTime")
    public String getOpeningTime() {
        return openingTime;
    }
    @JsonProperty("OpeningTime")
    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }
    @JsonProperty("ClosingTime")
    public String getClosingTime() {
        return closingTime;
    }
    @JsonProperty("ClosingTime")
    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }


}
