
package com.ikosmov.markup.json.example.data.brand;

import com.ikosmov.markup.json.example.data.brand.branch.Branch;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class Brand {

    private String brandName;
    private List<Branch> branch = null;
    @JsonProperty("BrandName")
    public String getBrandName() {
        return brandName;
    }
    @JsonProperty("BrandName")
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    @JsonProperty("Branch")
    public List<Branch> getBranch() {
        return branch;
    }
    @JsonProperty("Branch")
    public void setBranch(List<Branch> branch) {
        this.branch = branch;
    }

    @Override
    public String toString() {
        return "Brand{\n" +
                "brandName='" + brandName + '\'' +
                ", branch=" + branch +
                "\n}";
    }
}
