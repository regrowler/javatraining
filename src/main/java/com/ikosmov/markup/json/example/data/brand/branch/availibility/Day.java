
package com.ikosmov.markup.json.example.data.brand.branch.availibility;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class Day {

    private String name;
    private List<OpeningHour> openingHours = null;
    @JsonProperty("Name")
    public String getName() {
        return name;
    }
    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty("OpeningHours")
    public List<OpeningHour> getOpeningHours() {
        return openingHours;
    }
    @JsonProperty("OpeningHours")
    public void setOpeningHours(List<OpeningHour> openingHours) {
        this.openingHours = openingHours;
    }

}
