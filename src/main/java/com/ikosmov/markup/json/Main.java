package com.ikosmov.markup.json;



import com.ikosmov.markup.json.example.Pojo;
import org.codehaus.jackson.map.ObjectMapper;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;
import java.io.*;

public class Main {
    public static void main(String[] a){
        task2();
    }
    public static void task1(){
        try {
            FileReader reader=new FileReader("example.json");
            JsonReader parser= Json.createReader(reader);
            JsonObject object=parser.readObject();
            JsonArray array1=(JsonArray) object.getJsonArray("data");
            object=array1.getJsonObject(0);
            JsonArray array=(JsonArray) object.getJsonArray("Brand");
            JsonObject object1=(JsonObject) array.get(0);
            object1.forEach((s, jsonValue) -> {
                System.out.println("“Node with name "+s+" has" +
                        "type "+jsonValue.getValueType()+" and value: "+jsonValue.toString());
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static void task2(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            Pojo pojo = (Pojo) mapper.readValue(new FileInputStream("example.json"),Pojo.class);
            System.out.println(pojo);
            mapper.writeValue(new FileWriter("writen.json"),pojo);
            int y=0;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
