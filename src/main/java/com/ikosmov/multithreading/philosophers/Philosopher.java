package com.ikosmov.multithreading.philosophers;

public class Philosopher extends Thread {
    Fork leftFork;
    Fork rightFork;
    int eatCount;

    public Philosopher(Fork leftFork, Fork rightFork) {
        this.leftFork = leftFork;
        this.rightFork = rightFork;
        eatCount = 0;
    }

    @Override
    public void run() {
        try {
            while (true) {

                if (eatCount < 2) {
                    synchronized (leftFork) {
                        synchronized (rightFork) {
                            try {
                                eat();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }else {
                    Thread.sleep(1700);
                    eatCount=0;
                }
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }

    }

    void eat() throws InterruptedException {
        eatCount++;
        System.out.println(Thread.currentThread().getName() + " eating");
        Thread.sleep(800);
        System.out.println(Thread.currentThread().getName() + " finished eating");
    }
}
