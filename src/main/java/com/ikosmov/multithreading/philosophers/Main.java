package com.ikosmov.multithreading.philosophers;

public class Main {
    public static void main(String a[]){
        Fork fork1=new Fork();
        Fork fork2=new Fork();
        Fork fork3=new Fork();
        Fork fork4=new Fork();
        Fork fork5=new Fork();
        Philosopher philosopher1=new Philosopher(fork1,fork2);
        Philosopher philosopher2=new Philosopher(fork2,fork3);
        Philosopher philosopher3=new Philosopher(fork3,fork4);
        Philosopher philosopher4=new Philosopher(fork4,fork5);
        Philosopher philosopher5=new Philosopher(fork5,fork1);
        philosopher1.start();
        philosopher2.start();
        philosopher3.start();
        philosopher4.start();
        philosopher5.start();
    }
}
