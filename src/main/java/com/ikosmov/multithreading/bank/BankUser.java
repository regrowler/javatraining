package com.ikosmov.multithreading.bank;

public class BankUser extends Thread{
    long money;
    Bank bank;

    public BankUser(long money, Bank bank) {
        this.money = money;
        this.bank = bank;
    }

    @Override
    public void run() {
        try {
            if(bank.hasAmount(money)){
                System.out.println(Thread.currentThread().getName()+ " money "+money);
                bank.transferMoney(money);
            }
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }
}
