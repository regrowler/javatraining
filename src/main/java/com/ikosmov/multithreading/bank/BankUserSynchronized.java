package com.ikosmov.multithreading.bank;

public class BankUserSynchronized extends Thread{
    long money;
    Bank bank;

    public BankUserSynchronized(long money, Bank bank) {
        this.money = money;
        this.bank = bank;
    }

    @Override
    public void run() {
        try {
            if(bank.hasAmountSynchronized(money)){
                System.out.println(Thread.currentThread().getName()+ " money "+money);
                bank.transferMoneySynchronized(money);
            }
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }
}
