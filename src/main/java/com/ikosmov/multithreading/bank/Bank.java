package com.ikosmov.multithreading.bank;

import java.util.concurrent.locks.ReentrantLock;

public class Bank {
    private long amount;

    public Bank(long amount) {
        this.amount = amount;
    }

    public boolean hasAmount(long amount) {
        return this.amount >= amount;
    }

    public void transferMoney(long money) throws IllegalArgumentException {
        if (money > amount) throw new IllegalArgumentException();
        amount -= money;
    }

    public synchronized boolean hasAmountSynchronized(long amount) {
        return this.amount >= amount;
    }

    public synchronized void transferMoneySynchronized(long money) throws IllegalArgumentException {
        if (money > amount) throw new IllegalArgumentException();
        amount -= money;
    }

    public boolean hasAmount(long amount, ReentrantLock lock) {
        boolean res = false;
        lock.lock();
        res = this.amount >= amount;
        lock.unlock();
        return res;
    }

    public void transferMoney(long money, ReentrantLock lock) throws IllegalArgumentException {
        lock.lock();
        if (money > amount) throw new IllegalArgumentException();
        amount -= money;
        lock.unlock();
    }
}
