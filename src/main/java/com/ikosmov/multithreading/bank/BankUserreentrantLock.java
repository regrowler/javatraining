package com.ikosmov.multithreading.bank;

import java.util.concurrent.locks.ReentrantLock;

public class BankUserreentrantLock extends Thread{
    long money;
    Bank bank;
    ReentrantLock lock;

    public BankUserreentrantLock(long money, Bank bank, ReentrantLock lock) {
        this.money = money;
        this.bank = bank;
        this.lock = lock;
    }

    @Override
    public void run() {
        try {
            if(bank.hasAmount(money,lock)){
                System.out.println(Thread.currentThread().getName()+ " money "+money);
                bank.transferMoney(money,lock);
            }
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }
}
