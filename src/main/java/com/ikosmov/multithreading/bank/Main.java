package com.ikosmov.multithreading.bank;

import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] a) {
        reentrantLockTest();
    }

    static void dummyTest() {
        Bank bank = new Bank(10000);
        Stream.iterate(200, integer -> integer + 1000).limit(10)
                .forEach(integer -> (new BankUser(integer, bank)).start());
    }

    static void synchronizedTest() {
        Bank bank = new Bank(10000);
        Stream.iterate(200, integer -> integer + 1000).limit(10)
                .forEach(integer -> (new BankUser(integer, bank)).start());
    }
    static void reentrantLockTest() {
        Bank bank = new Bank(11000);
        ReentrantLock lock=new ReentrantLock();
        Stream.iterate(200, integer -> integer + 1000).limit(10)
                .forEach(integer -> (new BankUserreentrantLock(integer, bank,lock)).start());
    }

}
