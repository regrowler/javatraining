package com.ikosmov.reflection.task;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        ArrayList<Object> arrayList = new ArrayList<>();
        arrayList.add(new RandomClassRealization());
        arrayList.add(new RandomClassDeprecated());
        getDeprecated(arrayList);
    }

    public static List<Object> getDeprecated(Collection objects) {
        Iterator iterator = objects.iterator();
        List<Object> res = new ArrayList<>();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (isDeprecated(o)) {
                System.out.println(o.getClass().toString()+" is depricated");
                findAnalog(o.getClass());
                res.add(o);
            }
        }
        return res;
    }
    public static boolean isDeprecated(Object o){
        Class c = o.getClass();
        Object a = c.getAnnotation(Deprecated.class);
        return a!=null;
    }
    public static boolean isDeprecated(Class o){
        Object a = o.getAnnotation(Deprecated.class);
        return a!=null;
    }
    public static void findAnalog(Class c) {
        String res = "";
        //System.out.println(c.getPackage().toString());
        HashSet<String> set = new HashSet<>();
        Class gen = c.getSuperclass();
        Class[] inter = c.getInterfaces();
        if (gen != null && !Modifier.isAbstract(gen.getModifiers())) {
            res = gen.toString();
        } else if (gen != null && Modifier.isAbstract(gen.getModifiers())) {
            listDir(set, new File("."));
            set.stream().map(Main::loadClass)
                    .filter((op) -> op.isPresent())
                    .filter((op)->!isDeprecated(op.get()))
                    .filter((op)->{return compare(gen,op.get());})
                    .forEach((op) -> System.out.println("use " + op.get().toString() + " instead"));
        } else if (inter != null) {
            listDir(set, new File("."));
            set.stream().map(Main::loadClass)
                    .filter((op) -> op.isPresent())
                    .filter((op)->!isDeprecated(op.get()))
                    .filter((op)->{return compare(inter,op.get());})
                    .forEach((op) -> System.out.println("use " + op.get().toString() + " instead"));
        } else {
            System.out.println("no analog");
            return;
        }
        System.out.println(res);
    }
    public static boolean compare(Class gen,Class c2){
        if(!c2.isInterface()&&!Modifier.isAbstract(c2.getModifiers())){
            try {
                Object o=c2.getConstructor().newInstance();
                return gen.isInstance(o);
            } catch (NoSuchMethodException e) {
              //  e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
    public static boolean compare(Class[] gen,Class c2){
        if(!c2.isInterface()&&!Modifier.isAbstract(c2.getModifiers())){
            try {
                Object o=c2.getConstructor().newInstance();
                Stream.of(gen).forEach(aClass -> {if(aClass.isInstance(o))return;});
            } catch (NoSuchMethodException e) {
                //  e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
    public static Optional<Class> loadClass(String string){
        Optional<Class> optional= Optional.empty();
        try {
            Class s = ClassLoader.getSystemClassLoader().loadClass(string);
            optional = Optional.of(s);
        } catch (ClassNotFoundException e) {
            System.out.println(string);
            e.printStackTrace();
        }
        return optional;
    }
    private static void listDir(HashSet set, File file) {
        if (file.isDirectory()) {
            File[] folderEntries = file.listFiles();
            for (File file1 : folderEntries) {
                listDir(set, file1);
            }
        } else {
            // System.out.println(file.getName());
            if (file.getName().endsWith(".java")) {
                //System.out.println(file.getPath());

                set.add(toPackageView(file.getPath()).orElse(""));

            }
        }
    }

    public static Optional<String> toPackageView(String path) {
        Optional<String> s = Optional.empty();
        if (path.startsWith(".\\src\\main\\java\\")) {
            String tenp = path.replace(".\\src\\main\\java\\", "");
            tenp = tenp.replace("\\", ".");
            tenp = tenp.substring(0,tenp.length()-5);
            //System.out.println(tenp);
            s = Optional.of(tenp);
        } else {
            s = Optional.empty();
        }
        return s;
    }
}
