package com.ikosmov.patterns.payment;

import java.util.HashMap;

public class BonusManager {
    private HashMap<String, Long> clints;
    private final double percent = 0.1;

    public BonusManager() {
        clints = new HashMap<>();
    }
    public long totalCost(String phone, long cost){
        BonusAndCostWrapper bonusAndCostWrapper=process(phone,cost);
        System.out.println("total cost = " + bonusAndCostWrapper.getTotalCost());
        return bonusAndCostWrapper.getTotalCost();
    }
    private BonusAndCostWrapper process(String phone, long cost){
        Long availableBonus = clints.getOrDefault(phone, 0L);
        long totalCost = cost;
        if (availableBonus > 0 & cost > availableBonus) {
            totalCost -= availableBonus;
            System.out.println("wrote off " + availableBonus + " bonuses");
            availableBonus = Math.round(percent * totalCost);
        } else if (availableBonus > cost) {
            System.out.println("wrote off " + totalCost + " bonuses");
            availableBonus -= totalCost;
        } else {
            availableBonus = Math.round(percent * totalCost);
            System.out.println("wrote on " + availableBonus + " bonuses");
        }
        return new BonusAndCostWrapper(totalCost,availableBonus);
    }
    public long pay(String phone, long cost) {
        BonusAndCostWrapper bonusAndCostWrapper=process(phone,cost);
        clints.put(phone, bonusAndCostWrapper.getAvailableBonus());
        System.out.println("total cost = " + bonusAndCostWrapper.getTotalCost());
        return bonusAndCostWrapper.getTotalCost();
    }
    private class BonusAndCostWrapper{
        private long totalCost;
        private long availableBonus;

        public BonusAndCostWrapper(long totalCost, long availableBonus) {
            this.totalCost = totalCost;
            this.availableBonus = availableBonus;
        }

        public long getTotalCost() {
            return totalCost;
        }

        public long getAvailableBonus() {
            return availableBonus;
        }
    }
}
