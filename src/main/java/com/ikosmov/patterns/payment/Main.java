package com.ikosmov.patterns.payment;

import java.util.stream.Stream;

public class Main {
    public static void main(String[] a) {
        PaymentTerminal terminal = new PaymentTerminal();
        Stream.iterate(200, integer -> integer + 200).limit(10)
                .forEach(integer -> {
                    System.out.println("payed " +
                            terminal.payWithCard("89874344376", integer));
                    System.out.println("got back " +
                            terminal.payWithCash("89874344376", integer, integer + 500));
                });
    }
}
