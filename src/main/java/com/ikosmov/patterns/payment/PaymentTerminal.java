package com.ikosmov.patterns.payment;

public class PaymentTerminal {

    private BonusManager bonusManager;
    private CashPaymentMethod cashPaymentMethod;
    private CreditCardPaymentMethod creditCardPaymentMethod;

    public PaymentTerminal() {
        init();
    }

    private void init() {
        bonusManager = new BonusManager();
        cashPaymentMethod = new CashPaymentMethod();
        cashPaymentMethod.setManager(bonusManager);
        creditCardPaymentMethod = new CreditCardPaymentMethod();
        creditCardPaymentMethod.setManager(bonusManager);

    }

    public long payWithCard(String phone, long cost) {
        return creditCardPaymentMethod.pay(phone, cost);
    }

    public long payWithCash(String phone, long cost, long cash) {
        return cashPaymentMethod.pay(phone, cost, cash);
    }

}
