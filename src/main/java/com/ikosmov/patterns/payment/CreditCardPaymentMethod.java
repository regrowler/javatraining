package com.ikosmov.patterns.payment;

public class CreditCardPaymentMethod {
    private BonusManager manager;

    public void setManager(BonusManager manager) {
        this.manager = manager;
    }

    //ruturns total cost
    public long pay(String phone, long cost) {
        if (manager != null) {
            long totalCost = manager.pay(phone, cost);
            return totalCost;
        } else return cost;

    }
}
