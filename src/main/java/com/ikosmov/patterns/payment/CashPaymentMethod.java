package com.ikosmov.patterns.payment;

public class CashPaymentMethod {
    private BonusManager manager;

    public void setManager(BonusManager manager) {
        this.manager = manager;
    }

    //returns surrender
    public long pay(String phone, long cost, long cash) {
        if (manager != null) {
            long totalCost = manager.totalCost(phone, cost);
            if (totalCost <= cash) {
                manager.pay(phone, cost);
                return cash - totalCost;
            } else {
                System.out.println("not enough money");
                return cash;
            }
        } else {
            if(cash>=cost){
                return cash=cost;
            }else {
                System.out.println("not enough money");
                return cash;
            }
        }

    }
}
