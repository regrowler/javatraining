package com.ikosmov.patterns.pizza.dodoFactory;

import com.ikosmov.patterns.pizza.order.Drink;
import com.ikosmov.patterns.pizza.pizzaFactory.Bar;

public class DodoBar implements Bar {

    @Override
    public Drink getDrink(String name) {
        return new Drink(name);
    }
}
