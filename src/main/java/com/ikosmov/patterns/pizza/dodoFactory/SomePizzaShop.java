package com.ikosmov.patterns.pizza.dodoFactory;

import com.ikosmov.patterns.pizza.order.Pizza;
import com.ikosmov.patterns.pizza.pizzaFactory.PizzaShop;

public class SomePizzaShop implements PizzaShop {

    @Override
    public Pizza buyPizza(String name) {
        return new Pizza(name);
    }
}
