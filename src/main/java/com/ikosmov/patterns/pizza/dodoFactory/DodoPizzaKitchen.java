package com.ikosmov.patterns.pizza.dodoFactory;

import com.ikosmov.patterns.pizza.order.Pizza;
import com.ikosmov.patterns.pizza.pizzaFactory.Kitchen;

public class DodoPizzaKitchen implements Kitchen {
    @Override
    public Pizza getPizza(String name) {
        return new Pizza(name);
    }
}
