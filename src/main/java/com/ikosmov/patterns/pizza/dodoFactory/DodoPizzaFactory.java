package com.ikosmov.patterns.pizza.dodoFactory;

import com.ikosmov.patterns.pizza.order.Drink;
import com.ikosmov.patterns.pizza.order.Order;
import com.ikosmov.patterns.pizza.order.Pizza;
import com.ikosmov.patterns.pizza.pizzaFactory.Bar;
import com.ikosmov.patterns.pizza.pizzaFactory.Kitchen;
import com.ikosmov.patterns.pizza.pizzaFactory.PizzaFactory;
import com.ikosmov.patterns.pizza.pizzaFactory.PizzaShop;

public class DodoPizzaFactory implements PizzaFactory {
    Bar bar;
    Kitchen kitchen;

    public DodoPizzaFactory(Bar bar, Kitchen kitchen) {
        this.bar = bar;
        this.kitchen = kitchen;
    }

    @Override
    public Order takeOrder(String name, Order.OrderType type)  {
        try {
            if (type == Order.OrderType.PIZZA) {
                return makePizza(name);
            } else if (type == Order.OrderType.DRINK) {
                return makeDrink(name);
            }
        }catch (Exception e){
            System.out.println("err occured: "+e.getMessage());
        }

        return null;
    }

    private Drink makeDrink(String name) throws InternalError {
        if (bar == null) {
            throw new InternalError("bar not attached");
        }
        return bar.getDrink(name);
    }

    private Pizza makePizza(String name) throws Exception {
        if (kitchen == null) {
            throw new Exception("kitchen not attached");
        } else return kitchen.getPizza(name);
    }
}
