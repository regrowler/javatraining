package com.ikosmov.patterns.pizza.order;

public class Order {
    String name;

    public Order(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public enum OrderType {
        PIZZA, DRINK
    }
}
