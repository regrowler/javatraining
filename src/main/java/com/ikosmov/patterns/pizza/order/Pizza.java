package com.ikosmov.patterns.pizza.order;

public class Pizza extends Order {

    public Pizza(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "pizza{" +
                "name='" + name + '\'' +
                '}';
    }
}
