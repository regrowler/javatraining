package com.ikosmov.patterns.pizza.order;

public class Drink extends Order {


    public Drink(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Drink{" +
                "name='" + name + '\'' +
                '}';
    }
}
