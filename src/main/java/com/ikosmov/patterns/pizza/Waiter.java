package com.ikosmov.patterns.pizza;

import com.ikosmov.patterns.pizza.order.Order;
import com.ikosmov.patterns.pizza.pizzaFactory.PizzaFactory;

public class Waiter {
    PizzaFactory pizzaFactory;

    public Waiter(PizzaFactory pizzaFactory) {
        this.pizzaFactory = pizzaFactory;
    }

    public void takeOrder(Order.OrderType type, String name) {
        System.out.println(pizzaFactory.takeOrder(name, type));
    }

    public void takeDrinkOrder(String name) {
        System.out.println(pizzaFactory.takeDrinkOrder(name));
    }

    public void takePizzaOrder(String name) {
        System.out.println(pizzaFactory.takePizzaOrder(name));
    }
}
