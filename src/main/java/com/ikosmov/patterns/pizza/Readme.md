##Pizza restaurant task  
Chose a abstract factory pattern 
Bar, Kitchen, PizzaFactory are interface's which work with each other  
When we implement our restaurant we can implement bar and kitchen to choose how will they work.  
Also we have classes Order, Pizza and Drink.  
Class Order contains only enum with types of orders.  
Pizza and Drink are children of Order. So if want another type ex. sushi we just creates class and add new type to enum.
