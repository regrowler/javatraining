package com.ikosmov.patterns.pizza;

import com.ikosmov.patterns.pizza.dodoFactory.DodoBar;
import com.ikosmov.patterns.pizza.dodoFactory.DodoPizzaFactory;
import com.ikosmov.patterns.pizza.dodoFactory.DodoPizzaKitchen;
import com.ikosmov.patterns.pizza.dodoFactory.SomePizzaShop;
import com.ikosmov.patterns.pizza.pizzaFactory.PizzaFactory;
import com.ikosmov.patterns.pizza.pizzaFactory.PizzaShopToKitchenAdapter;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] a) {
        PizzaFactory f = new DodoPizzaFactory(new DodoBar(), new DodoPizzaKitchen());
        PizzaFactory f2 = new DodoPizzaFactory(new DodoBar(), new PizzaShopToKitchenAdapter(new SomePizzaShop()));
        Waiter waiter1 = new Waiter(f);
        Waiter waiter2 = new Waiter(f2);
        IntStream.range(0, 10).forEach(integer -> {
            waiter1.takeDrinkOrder("drink  " + integer);
            waiter1.takePizzaOrder("pizza  " + integer);
            waiter2.takeDrinkOrder("drink2 " + integer);
            waiter2.takePizzaOrder("pizza2 " + integer);
        });
    }
}
