package com.ikosmov.patterns.pizza.pizzaFactory;

import com.ikosmov.patterns.pizza.order.Drink;
import com.ikosmov.patterns.pizza.order.Order;
import com.ikosmov.patterns.pizza.order.Pizza;

public interface PizzaFactory {
    Order takeOrder(String name, Order.OrderType type);

    public default Pizza takePizzaOrder(String name) {
        return (Pizza) takeOrder(name, Order.OrderType.PIZZA);
    }

    public default Drink takeDrinkOrder(String name) {
        return (Drink) takeOrder(name, Order.OrderType.DRINK);
    }

}
