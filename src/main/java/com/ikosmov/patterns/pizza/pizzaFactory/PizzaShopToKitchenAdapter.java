package com.ikosmov.patterns.pizza.pizzaFactory;

import com.ikosmov.patterns.pizza.order.Pizza;

public class PizzaShopToKitchenAdapter implements Kitchen{
    private PizzaShop shop;

    public PizzaShopToKitchenAdapter(PizzaShop shop) {
        this.shop = shop;
    }

    @Override
    public Pizza getPizza(String name) {
        return shop.buyPizza(name);
    }
}
