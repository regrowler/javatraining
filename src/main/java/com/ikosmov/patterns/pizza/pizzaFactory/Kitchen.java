package com.ikosmov.patterns.pizza.pizzaFactory;

import com.ikosmov.patterns.pizza.order.Pizza;

public interface Kitchen {
    public Pizza getPizza(String name);
}
