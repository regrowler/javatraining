package com.ikosmov.patterns.pizza.pizzaFactory;

import com.ikosmov.patterns.pizza.order.Pizza;

public interface PizzaShop {
    public Pizza buyPizza(String name);
}
