package com.ikosmov.patterns.pizza.pizzaFactory;

import com.ikosmov.patterns.pizza.order.Drink;

public interface Bar {
    public Drink getDrink(String name);
}
