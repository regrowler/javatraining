package com.ikosmov.generics;

import java.util.Arrays;
import java.util.Collection;

public class OrderProcessor {
    public static <O extends Order>void printOrders(Collection<O> o){
        System.out.println(Arrays.toString(o.toArray()));
    }
}
