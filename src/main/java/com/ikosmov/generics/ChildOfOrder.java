package com.ikosmov.generics;

public class ChildOfOrder extends Order {
    public ChildOfOrder(OrderStatus status, int id) {
        super(status, id);
    }

    @Override
    public String toString() {
        return id+status.toString();
    }
}
