package com.ikosmov.generics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Collection<Order> ordersnew = Stream.iterate(1,integer -> integer+2).map(integer -> new Order(Order.OrderStatus.COMPLETED,integer)).limit(6).collect(Collectors.toList());
        Collection<ChildOfOrder> children = Stream.iterate(1,integer -> integer+2).map(integer -> new ChildOfOrder(Order.OrderStatus.COMPLETED,integer)).limit(6).collect(Collectors.toList());
        OrderProcessor.printOrders(ordersnew);
        OrderProcessor.printOrders(children);

    }


}
