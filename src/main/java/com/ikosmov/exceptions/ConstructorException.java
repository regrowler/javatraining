package com.ikosmov.exceptions;

public class ConstructorException {
    String s;

    public ConstructorException(String s) throws IllegalArgumentException{
        if(s==null)throw new IllegalArgumentException();
        this.s = s;
    }
}
