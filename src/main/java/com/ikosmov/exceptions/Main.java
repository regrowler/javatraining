package com.ikosmov.exceptions;

public class Main {

    public static void main(String[] args) {
        try {
            new LifeCycleAction().execute();
        } catch (LifeCycleActionExecutionException | WorldWarException e) {
            System.err.println(e.getLocalizedMessage());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
//        try (FileInputStream fileInputStream = new FileInputStream(args[0])) {
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        try (AutoClose autoClose=new AutoClose()){

        } catch (WorldWarException e) {
            e.printStackTrace();
        }
        ConstructorException exception=new ConstructorException("asd");
        try {
            exception=new ConstructorException(null);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
        int y=0;
    }

    public static class LifeCycleAction {
        public void execute() throws LifeCycleActionExecutionException,WorldWarException {
            throw new LifeCycleActionExecutionException();
        }
    }

    public static class LifeCycleActionExecutionException extends Exception {
    }
    public static class WorldWarException extends Exception{}


}
