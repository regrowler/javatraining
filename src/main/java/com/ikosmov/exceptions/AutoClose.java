package com.ikosmov.exceptions;

public class AutoClose implements AutoCloseable {
    LoggerThread thread;

    public AutoClose() {
        thread = new LoggerThread();
//        thread.start();
    }

    @Override
    public void close() throws Main.WorldWarException {
        System.err.println("closed");
        thread.interrupt();
    }

    private class LoggerThread extends Thread {
        @Override
        public void run() {
            try {
                while (true) {
                    Thread.sleep(100);
                    System.out.println("c urr time is " + System.currentTimeMillis());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
