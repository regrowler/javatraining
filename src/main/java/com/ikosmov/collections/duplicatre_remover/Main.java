package com.ikosmov.collections.duplicatre_remover;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> t = new ArrayList();
        for (int i = 0; i < 50; i++) {
            t.add(i);
        }
        for (int i = 51; i >= 0; i--) {
            t.add(i);
        }
        removeDuplicates2(t);
        for (Integer i : t) {
            System.out.println(i);
        }
        int y = 0;
    }

    public static void removeDuplicates2(Collection collection) {
        if (collection == null) return;
        HashSet<Object> set = new HashSet<>();
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            set.add(iterator.next());
        }
        collection.clear();
        collection.addAll(set);
    }
}
