##Task 1
Traditional cycle via for is indispensable for producing something.
But for processing of data that already exists my favourite option is making cycle via stream Api.
The main reason is that we use speaking named functions and it makes code much easier to read.
Also we can edit objects in streams that we can't to in foreach cycle.
We can use functions that provide another streams. It makes nested cycles more readable.  

In another way foreach cycle is much easier to write simple things but it has some restrictions.
Traditional cycle via for is easy to write and not so hard to read but it makes code bigger.
So we see less information on screen. 
    