package com.ikosmov.collections.mapcache;

import java.util.*;

public class CacheMap {
    private HashMap<Object, ObjectWrapper> m;

    public CacheMap() {
        m = new HashMap<>();
    }

    public void putObject(Object key, Object value) {
        m.put(key, new ObjectWrapper(value));
    }

    public void clear() {
        m.clear();
    }

    public Object getObject(Object key) {
        return m.get(key).object;
    }

    private class ObjectWrapper {
        Object object;
        long time;

        public ObjectWrapper(Object object) {
            this.object = object;
            time = System.currentTimeMillis();
        }
    }

    public void invalidate() {
        List<Object> toRemove = new ArrayList<>();
        m.forEach(((o, objectWrapper) -> {
            long d = System.currentTimeMillis() - objectWrapper.time;
            if (d > (5 * 60 * 1000)) {
                toRemove.add(o);
                System.out.println("deletedn " + o.toString());
            }
        }));
        toRemove.forEach((o -> m.remove(o)));
    }

    public void invalidate(Object key) {
        long d = System.currentTimeMillis() - m.get(key).time;
        if (d > (5 * 60 * 1000)) {
            m.remove(key);
            System.out.println("deletedn " + key);
        }
    }
}
