package com.ikosmov.collections.requestCoordinator;

public class Processor extends Thread {
    Request request;
    Coordinator coordinator;
    boolean flag = false;

    public boolean isProcessing() {
        return flag;
    }

    public Processor() {
//        start();
    }

    public void takeTask(Request request) {
        this.request = request;
        System.out.println("processor: started");
        flag = true;

    }

    @Override
    public void run() {
        try {
            System.out.println("proceessor: started");
            while (true) {
                //System.err.println("processor: processing "+flag);
                if (flag) {
                    //System.err.println("processor: flag true");
                    if (request != null) {
                        Thread.sleep(request.processing);
                        System.out.println("processed "+request.processing);

                    }
                    flag = false;
                }else Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("finish");
        }

    }

}
