package com.ikosmov.collections.requestCoordinator;

import java.util.stream.Stream;

public class RequestGiver extends Thread {
    Coordinator coordinator;

    public RequestGiver(Coordinator coordinator) throws IllegalArgumentException{
        if(coordinator==null)throw new IllegalArgumentException();
        this.coordinator = coordinator;
        start();
    }

    @Override
    public void run() {
        Stream<Request>s=Stream.generate(()->{return new Request(300);});
        s.forEach(r->{
            coordinator.takeTask(r);
            try {
                Thread.sleep(200);
            }catch (InterruptedException e){e.printStackTrace();}
        });
//
    }
}
