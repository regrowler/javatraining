package com.ikosmov.collections.requestCoordinator;

public interface Coordinator {
    public void takeTask(Request request);
}
