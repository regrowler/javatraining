package com.ikosmov.collections.requestCoordinator;

import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.Stream;

public class ThreadCoordinator extends Thread implements Coordinator {
    Deque<Request> requests;
    Object[] processors;

    public ThreadCoordinator() {
        requests = new LinkedList<>();
        processors = Stream.generate(() -> new Processor()).limit(3).toArray();

    }

    @Override
    public void run() {
        super.run();
        System.out.println("ThreadCoordinator: started listening for requests");
        while (true) {
            for (Object proc:processors){
                Processor processor = (Processor) proc;
                if (!processor.isProcessing() && requests.size() > 0) {
                    processor.takeTask(requests.pollFirst());

                }
            }
        }
    }

    @Override
    public synchronized void takeTask(Request request) {
        System.out.println("ThreadCoordinator: received task");
        requests.addLast(request);
    }
}
