package com.ikosmov.collections.benchmark;

import java.util.HashSet;
import java.util.TreeSet;

public class BenchMark {
    public static void bench(){
        TreeSet<Integer> treeSet=new TreeSet<>();
        HashSet<Integer> hashSet=new HashSet<>();
        long cur=System.currentTimeMillis();
        long sl=0;
        for(int i=0;i<1000000;i++){
            treeSet.add(i);
        }
        sl=System.currentTimeMillis();
        System.err.println("treeSet add 1 000 000 in "+((sl-cur)/1000d));
        cur=System.currentTimeMillis();
        sl=0;
        for(int i=0;i<1000000;i++){
            hashSet.add(i);
        }
        sl=System.currentTimeMillis();
        System.err.println("hashSet add 1 000 000 in "+((sl-cur)/1000d));

        cur=System.currentTimeMillis();
        sl=0;
        for(int i=1000000;i>=0;i--){
            treeSet.remove(i);
        }
        sl=System.currentTimeMillis();
        System.err.println("treeSet remove 1 000 000 in "+((sl-cur)/1000d));

        cur=System.currentTimeMillis();
        sl=0;
        for(int i=1000000;i>=0;i--){
            hashSet.remove(i);
        }
        sl=System.currentTimeMillis();
        System.err.println("hashSet remove 1 000 000 in "+((sl-cur)/1000d));


        cur=System.currentTimeMillis();
        sl=0;
        for(int i=1000000;i>=0;i--){
            treeSet.contains(i);
        }
        sl=System.currentTimeMillis();
        System.err.println("treeSet contain 1 000 000 in "+((sl-cur)/1000d));

        cur=System.currentTimeMillis();
        sl=0;
        for(int i=1000000;i>=0;i--){
            hashSet.contains(i);
        }
        sl=System.currentTimeMillis();
        System.err.println("hashSet contain 1 000 000 in "+((sl-cur)/1000d));
    }
}
