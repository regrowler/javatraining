package com.ikosmov.io;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] a) {
        task5();
    }

    public static void task1() {
        try (BufferedReader reader = new BufferedReader(new FileReader("text.txt"))) {
            StringBuilder builder = new StringBuilder();
            reader.lines().forEach(s -> {
                builder.append(s);
                builder.append("\n");
            });
            builder.reverse();
            System.out.println(builder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void task2() {

        try (BufferedReader reader = new BufferedReader(new FileReader("text.txt"))) {
            StringBuilder builder = new StringBuilder();
            reader.lines().map(s -> {
                return new StringBuilder(s).reverse().toString();
            })
                    .forEach(s -> {
                        builder.append(s);
                        builder.append("\n");
                    });
            builder.reverse();
            System.out.println(builder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void task3() {
        try (DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test.dat")))) {
            dataOutputStream.writeUTF("Donut");
            dataOutputStream.writeDouble(20.0);
            dataOutputStream.writeInt(22);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream("test.dat")))) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(dataInputStream.readUTF());
            stringBuilder.append(" ");
            stringBuilder.append(dataInputStream.readDouble());
            stringBuilder.append(" ");
            stringBuilder.append(dataInputStream.readInt());
            System.out.println(stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void task4() {
        ObjectToSerialize object1 = new ObjectToSerialize();
        ObjectToSerialize.Link link = new ObjectToSerialize.Link();
        link.setBuilder(new StringBuilder());
        StringBuilder builder = new StringBuilder();
        object1.setStringBuilder(builder);
        ObjectToSerialize object2 = new ObjectToSerialize();
        object2.setStringBuilder(builder);
        object2.setName("obj2");
        object1.setName("obj1");
        object1.setLink(link);
        object2.setLink(link);

        //write obj1
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("obj1.ser"))) {
            out.writeObject(object1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //write obj2
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("obj2.ser"))) {
            out.writeObject(object2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //read object1
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("obj1.ser"))) {
            object1 = (ObjectToSerialize) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        //read object1
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("obj2.ser"))) {
            object2 = (ObjectToSerialize) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("obj1 " + object1.getName());
        System.out.println("obj2 " + object1.getName());
        System.out.println("obj1 sb " + object1.getStringBuilder().toString());
        System.out.println("obj2 sb " + object2.getStringBuilder().toString());
        System.out.println("obj1 link " + object1.getLink().toString());
        System.out.println("obj2 link " + object2.getLink().toString());
        System.out.println("stringbuilders " + object1.getStringBuilder().equals(object2.getStringBuilder()));
        System.out.println("links " + object1.getLink().equals(object2.getLink()));
        System.out.println("stringbuilders in links " + object1.getLink().getBuilder().equals(object2.getLink().getBuilder()));

    }

    public static void task5() {
        System.out.println("TXT READER");
        Scanner scanner = new Scanner(System.in);
        FileReader fileReader = null;
        try {
            while (fileReader == null) {
                System.out.println("Enter file name");
                String s = scanner.nextLine();

                fileReader = new FileReader(s);
            }
            BufferedReader reader = new BufferedReader(fileReader);
            String line = reader.readLine();
            while (line != null) {
                System.out.println(line);
                line = reader.readLine();
                scanner.nextLine();
            }
            reader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("smth went wrong");
            e.printStackTrace();
        }

    }
}
